import React, { Component } from 'react';

class Counter extends Component {
  state = {
    count: 100,
    tags: ['tag1', 'tag2', 'tag3']
  };

  RenderTags() {
    return this.state.tags.length === 0 ?
      <p>there're no tags</p> :
      <ul>{this.state.tags.map(tag => <li key={tag}>{tag}</li>)}</ul>;
  }

  incmtHdlr(arg) {
    console.log('Increment clicked', arg);
  }
  
  render() {
    return (
      <React.Fragment>
        <span className={this.GetClass()}> { this.Change() } </span>
        <button onClick={() => { this.incmtHdlr({id:1}) }} className="btn btn-primary">Increment</button>
        { this.RenderTags() }
      </React.Fragment>
    )
  }

  GetClass() {
    let classes = "badge m-2 badge-";
    classes += this.state.count === 0 ? "warning" : "primary";
    return classes;
  }

  Change() {
    return this.state.count === 0 ? 'Zero' : this.state.count;
  }
}

export default Counter;
